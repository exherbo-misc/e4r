" vim: set sw=4 sts=4 et :
"
" Copyright (c) 2009 Ciaran McCreesh. Distributed under the terms of the GNU
" General Public License, version 2.
"
" This is e4r, a script that turns Vim into a very simple non-modal editor
" that somewhat resembles 'nano'.
"

scriptencoding utf-8
set nocompatible

if &term =~ "xterm"
    " use xterm titles
    if has('title')
        set title
        set titlestring=%f%h%m%r%w\ -\ e4r
    endif
endif

set backspace=indent,eol,start
set showcmd
set showmatch
set hlsearch
set incsearch
nohls
set ignorecase
set showfulltag
set scrolloff=3
set sidescrolloff=2
set wildmenu
set virtualedit=block,onemore
set nomodeline

set laststatus=2
set statusline=
set statusline+=%f\ %1*%m%r%0*%=[^W:\ Write]\ [^X:\ Exit]\ [F1:\ Help]

inoremap <C-w> <C-o>:call E4RPromptAndSave()<CR>
inoremap <C-x> <C-o>:call E4RPromptAndQuit()<CR>
inoremap <F1>  <C-o>:call E4RHelp()<CR>

inoremap <C-k> <C-o>dd
inoremap <C-y> <C-o>P
inoremap <C-u> <C-o>u
inoremap <C-f> <C-o>/
inoremap <C-g> <C-o>/<CR>
inoremap <C-l> <C-o>:call E4RLoad()<CR>

fun! E4RPromptAndSave() abort
    call inputsave()
    let l:response = input("Enter filename to save, or blank to cancel: ", expand("%"), "file")
    call inputrestore()

    if l:response != ""
        exec "write " . l:response
    endif
endfun

fun! E4RPromptAndQuit() abort
    if &modified
        call inputsave()
        let l:response = input("Enter filename to save, or blank to discard changes: ", expand("%"), "file")
        call inputrestore()

        if l:response == ""
            quit!
        else
            exec "write " . l:response
            quit
        endif
    else
        quit
    endif
endfun

fun! E4RLoad() abort
    if &modified
        call inputsave()
        let l:response = input("Enter filename to save, or blank to discard changes: ", expand("%"), "file")
        call inputrestore()

        if l:response == ""
            bd!
        else
            exec "write " . l:response
            bd
        endif
    else
        bd
    endif

    call inputsave()
    let l:response = input("Enter filename to load: ", expand("%"), "file")
    call inputrestore()

    if l:response != ""
        exec "edit " . l:response
    endif
endfun

fun! E4RHelp() abort
    echo "Ctrl+F: Find text"
    echo "Ctrl+G: Repeat previous find"
    echo "Ctrl+K: Cut current line"
    echo "Ctrl+L: Load new file"
    echo "Ctrl+Y: Paste most recently cut line"
    echo "Ctrl+U: Undo"
    echo "Ctrl+W: Write file"
    echo "Ctrl+X: Exit"
endfun

colorscheme morning
syntax on
filetype on
filetype plugin on
filetype indent on
set insertmode

