VERSION = 1.1

PREFIX = /usr/local
BINDIR = $(PREFIX)/bin
DATADIR = $(PREFIX)/share

all : e4r e4r.vim

install : e4r e4r.vim
	install -D e4r $(DESTDIR)$(BINDIR)/e4r
	install -m0644 -D e4r.vim $(DESTDIR)$(DATADIR)/e4r/e4r.vim

dist: e4r-$(VERSION).tar.bz2

e4r-$(VERSION).tar.bz2:
	@echo "Generating $@ ..."
	@git archive --prefix=e4r-$(VERSION)/ HEAD | bzip2 -c > $@

upload: e4r-$(VERSION).tar.bz2
	@echo "Uploading $< ..."
	scp $< dev.exherbo.org:/srv/www/dev.exherbo.org/distfiles/e4r/

